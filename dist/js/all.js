$(function() {

	// page load fade effect
	$('body').addClass('loaded');

	// home page sliders
	$('.home-quote').owlCarousel({
		animateOut: 'fadeOut',
		animateIn: 'fadeIn',
		items: 1,
		autoplay: true,
		autoplayTimeout: 5000,
		mouseDrag: false
	});

	$('.header__slider').owlCarousel({
		animateOut: 'fadeOut',
		animateIn: 'fadeIn',
		items: 1,
		autoplay: true,
		autoplayTimeout: 5000,
		mouseDrag: false
	});

	// accordions
	$('.vacancies__item-top').on('click', function(e) {
		if ($(this).parents('.vacancies__item').hasClass('active')) {
			$('.vacancies__item').removeClass('active');
		} else {
			$('.vacancies__item').removeClass('active');
			$(this).parents('.vacancies__item').toggleClass('active');
		}
	});

	$('.join-form__item-top').on('click', function(e) {
		if ($(this).parents('.join-form__item').hasClass('active')) {
			$('.join-form__item').removeClass('active');
		} else {
			$('.join-form__item').removeClass('active');
			$(this).parents('.join-form__item').toggleClass('active');
		}
	});

	// current date for training calendar
	var currentMonth = moment().format('YYYY-MM');
	var nextMonth = moment().add('month', 1).format('YYYY-MM');

	// Events for training calendar
	var events = [{
			date: currentMonth + '-' + '10',
			title: 'Persian Kitten Auction',
		},
		{
			date: currentMonth + '-' + '10',
			title: 'Cat Frisbee',
		},
		{
			date: currentMonth + '-' + '23',
			title: 'Kitten Demonstration',
		},
		{
			date: currentMonth + '-' + '23',
			title: 'Lorem ipsum dolor sit amet',
		}
	];

	// training calendar (requires jquery, underscore.js, moment.js and clndr.js)
	if ($("#mini-clndr").length) {
		$('#mini-clndr').clndr({
			template: $('#mini-clndr-template').html(),
			events: events,
			weekOffset: 1, // start on monday
			adjacentDaysChangeMonth: true, // display prev and next monts days
			daysOfTheWeek: ['Sun', 'Mon', 'Tues', 'Wed', 'Thurs', 'Fri', 'Sat'],
			targets: {
				nextButton: 'clndr-next-button',
				previousButton: 'clndr-previous-button'
			}
		});
	};

	$('.training-calendar .day.event').on('click', function(){
		if ( $(this).hasClass('active') ) {
			$(this).removeClass('active');
		} else {
			$('.training-calendar .day.event').removeClass('active');
			$(this).addClass('active');
		}
	});

	$(".header__nav-link").on("click", function(e){
		if ( $(window).width() < 1450 ) {
			e.preventDefault();
			$(this).toggleClass('active');
		}
	});

	$('.header__burger').on('click', function(e){
		e.preventDefault();
		$('.header__nav').toggleClass('active');
		$('.wrapper').toggleClass('locked');
	})

});